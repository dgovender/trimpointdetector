# TrimpointDetector

Algorithm that utilises pyAudioAnalysis to detect the start and end trim points for Opencast recorded lectures.

# Dependencies

pyAudioAnalysis (https://github.com/tyiannak/pyAudioAnalysis)
